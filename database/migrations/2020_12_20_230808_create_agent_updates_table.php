<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_updates', function (Blueprint $table) {
            $table->id();
            $table->foreignId('agent_id')->nullable()->constrained('agents')->onDelete('set null');
            $table->foreignId('user_id')->nullable()->constrained('users')->onDelete('set null');
            $table->string('username')->nullable();
            $table->string('name')->nullable();
            $table->string('wallet_num',100)->nullable();
            $table->string('email', 50)->nullable();
            $table->string('phone', 20)->nullable();
            $table->string('dni', 30)->nullable();
            $table->string('old_username')->nullable();
            $table->string('old_name')->nullable();
            $table->string('old_wallet_num',100)->nullable();
            $table->string('old_email', 50)->nullable();
            $table->string('old_phone', 20)->nullable();
            $table->string('old_dni', 30)->nullable();
            $table->text('id_f')->nullable()->comment('url dni front photo');
            $table->text('id_b')->nullable()->comment('url dni back photo');
            $table->date('old_birthdate', 20)->nullable();
            $table->text('term_selfie')->nullable()->comment('url photo term selfie with dni');
            $table->text('auth_image')->nullable()->comment('url photo document to accept the terms.');
            $table->decimal('discount', 5,3)->nullable()->default(0.000);
            $table->boolean('active')->default(1);
            $table->text('user_ip')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_updates');
    }
}
