<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePercentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('percents', function (Blueprint $table) {
            $table->id();
            $table->string('type', 50);
            $table->decimal('amount',10);
            $table->boolean('active')->default(0);
            $table->timestamps();
        });

        DB::table('percents')->insert(
            ['type' =>'Inical', 'amount' => 4.5, 'active'=>1]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('percents');
    }
}
