<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNetBtcToSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales', function (Blueprint $table) {
            $table->decimal('net_btc', 10,2)->nullable();
            $table->decimal('gross_btc', 10,2)->nullable();
            $table->decimal('profits_btc', 10,8)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales', function (Blueprint $table) {
          $table->dropColumn(['net_btc', 'gross_btc','profits_btc']);
        });
    }
}
