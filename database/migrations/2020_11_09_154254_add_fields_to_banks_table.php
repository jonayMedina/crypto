<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('banks', function (Blueprint $table) {
            $table->string('email',100)->nullable()->after('active');
            $table->string('dni_holder',20)->nullable()->after('email');
            $table->string('phone_holder', 20)->after('dni_holder')->nullable();
            $table->text('address')->after('phone_holder')->nullable();
            $table->string('office_num', 20)->after('address')->nullable();
            $table->string('bci')->after('office_num')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('banks', function (Blueprint $table) {
          $table->dropColumn(['email', 'dni_holder', 'phone_holder', 'address_holer', 'office_num', 'bci']);
        });
    }
}
