<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([ 'middleware' => 'api',  'prefix' => 'auth' ], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('me', 'AuthController@me');
});
Route::group(['middleware' => 'auth:api'], function () {

    Route::group(['prefix' => 'countries'], function () {
        Route::get('/', 'CountryController@index');
        Route::post('store', 'CountryController@store');
        Route::get('edit/{id}', 'CountryController@edit');
        Route::put('update/{id}', 'CountryController@update');
        Route::put('desactive/{id}', 'CountryController@desactive');
        Route::put('activate/{id}', 'CountryController@activate');
        Route::delete('destroy/{id}', 'CountryController@destroy');
    });

    Route::group(['prefix' => 'pays'], function () {
        Route::get('agent-count/{id}', 'PayController@agentCount');
        Route::post('store', 'PayController@store');
        Route::get('edit/{id}', 'PayController@edit');
        Route::get('agent/{id}', 'PayController@indexAgent');
        Route::put('update/{id}', 'PayController@update');
        Route::put('desactive/{id}', 'PayController@desactive');
        Route::put('activate/{id}', 'PayController@activate');
        Route::delete('destroy/{id}', 'PayController@destroy');
        Route::get('pays/img/{id}', 'PayController@imgDownload');
    });

    Route::group(['prefix' => 'sales'], function () {
        Route::get('/', 'SaleController@index');
        Route::get('count', 'SaleController@indexCount');
        Route::get('agent-count/{id}', 'SaleController@agentCount');
        Route::get('history', 'SaleController@indexHistory');
        Route::post('store', 'SaleController@store');
        Route::get('agent/{id}', 'SaleController@indexAgent');
        Route::get('show/{id}', 'SaleController@show');
        Route::get('edit/{id}', 'SaleController@edit');
        Route::put('update/{id}', 'SaleController@update');
        Route::put('desactive/{id}', 'SaleController@desactive');
        Route::put('activate/{id}', 'SaleController@activate');
        Route::delete('destroy/{id}', 'SaleController@destroy');
    });


    Route::group(['prefix' => 'clients'], function () {
        Route::get('/', 'ClientController@index');
        Route::post('store', 'ClientController@store');
        Route::get('count', 'ClientController@indexCount');
        Route::get('agent-clients/', 'ClientController@agentClients');
        Route::get('show/{id}', 'ClientController@show');
        Route::get('search/', 'ClientController@search');
        Route::get('search-admin/{name}', 'ClientController@searchAdmin');
        Route::get('edit/{id}', 'ClientController@edit');
        Route::get('clients-approveds', 'ClientController@clientsApproveds');
        Route::put('update/{id}', 'ClientController@update');
        Route::put('approve/{id}', 'ClientController@approve');
        Route::put('desactive', 'ClientController@desactive');
        Route::put('activate/{id}', 'ClientController@activate');
        Route::delete('destroy/{id}', 'ClientController@destroy');
        Route::get('email-verify/{email}', 'ClientController@emailV');
        Route::get('dni-verify/{dni}', 'ClientController@dniV');
        Route::get('phone-verify/{phone}', 'ClientController@phoneV');

        Route::get('total/', 'ClientController@totalCalculate');
    });

    Route::get('agents', 'AgentController@index');
    Route::group(['prefix' => 'agents'], function () {
        Route::post('store', 'AgentController@store');
        Route::get('show/{id}', 'AgentController@show');
        Route::get('get/{id}', 'AgentController@get');
        Route::get('edit/{id}', 'AgentController@edit');
        Route::put('update/{id}', 'AgentController@update');
        Route::put('desactive/{id}', 'AgentController@desactive');
        Route::put('activate/{id}   ', 'AgentController@activate');
        Route::delete('destroy/{id}', 'AgentController@destroy');
        Route::get('agent-clients/', 'AgentController@agentClients');
        Route::put('password/{id}', 'AgentController@newPassword');
        Route::post('email-verify', 'AgentController@email');
        Route::post('user-verify', 'AgentController@user');
    });

    Route::get('agent-preferences', 'AgentPreferencesController@get');
    Route::post('agent-preferences-store', 'AgentPreferencesController@store');
    Route::put('agent-preferences-update', 'AgentPreferencesController@update');

    Route::group(['prefix' => 'agent-updates'], function () {
        Route::get('/', 'AgentUpdateController@index');
    });

    Route::get('banks', 'BankController@index');
    Route::group(['prefix' => 'banks'], function () {
        Route::get('active', 'BankController@indexActive');
        Route::post('store', 'BankController@store');
        Route::get('show/{id}', 'BankController@show');
        Route::get('edit/{id}', 'BankController@edit');
        Route::post('update/{id}', 'BankController@update');
        Route::post('desactive', 'BankController@desactive');
        Route::post('activate', 'BankController@activate');
        Route::delete('destroy/{id}', 'BankController@destroy');
    });

    Route::get('prices', 'PriceController@index');
    Route::group(['prefix' => 'price'], function () {
        Route::post('store', 'PriceController@store');
        Route::get('show/{id}', 'PriceController@show');
        Route::get('eur', 'PriceController@getEur');
        // Route::get('edit/{id}', 'PriceController@edit');
        Route::put('update/{id}', 'PriceController@update');
        // Route::post('desactive', 'PriceController@desactive');
        Route::put('activate', 'PriceController@activate');
        // Route::delete('destroy/{id}', 'PriceController@destroy');
    });

    Route::group(['prefix' => 'currencies'], function () {
        Route::get('/', 'CurrencyController@index');
        Route::post('store', 'CurrencyController@store');
        Route::get('show/{currency}', 'CurrencyController@show');
        // Route::get('edit/{id}', 'CurrencyController@edit');
        Route::put('update/{currency}', 'CurrencyController@update');
        Route::put('desactive/{currency}', 'CurrencyController@desactive');
        Route::put('activate/{currency}', 'CurrencyController@activate');
        Route::delete('destroy/{currency}', 'CurrencyController@destroy');
    });

    Route::group(['prefix' => 'balances'], function (){
        Route::get('/', 'BalanceController@index');
        Route::get('active', 'BalanceController@active');
        Route::post('store', 'BalanceController@store');
        Route::put('modif/{balance}', 'BalanceController@modif');
        Route::put('update/{balance}', 'BalanceController@update');
        Route::put('add-btc/{balance}', 'BalanceController@addBtc');
        Route::put('desactive/{balance}', 'BalanceController@desactive');
        Route::put('activate/{balance}', 'BalanceController@activate');
        Route::delete('destroy/{balance}', 'BalanceController@destroy');
    });

    Route::get('percents', 'PercentController@index');
    Route::group(['prefix' => 'percent'], function () {
        Route::post('store', 'PercentController@store');
        Route::get('show/{id}', 'PercentController@show');
        // Route::get('edit/{id}', 'PercentController@edit');
        Route::post('update/{id}', 'PercentController@update');
        Route::put('activate/{percent}', 'PercentController@activate');
        Route::get('get', 'PercentController@getActive');
        Route::get('set', 'PercentController@setPrice');
    });

});
Route::get('price/get', 'PriceController@getPrice');
Route::get('price/set', 'PriceController@setPrice');
Route::get('sales/img/{id}', 'SaleController@imgDownload');
// searchs

