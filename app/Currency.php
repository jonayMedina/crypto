<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Currency extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'iso', 'symbol', 'active'];

    protected $casts = [
	    'created_at' => 'datetime:H:i:s d/m/Y ', // Change format
	    'updated_at' => 'datetime:H:i:s d/m/Y',
    ];

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }
}
