<?php

namespace App\Repositories;

use App\Client;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ClientRepository implements ClientRepositoryInterface
{
    protected $model;

    /**
     * clientRepository constructor.
     *
     * @param client $client
     */
    public function __construct(Client $client)
    {
        $this->model = $client;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function update(array $data, $id)
    {
        return $this->model->where('id', $id)
            ->update($data);
    }

    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    public function find($id)
    {
        if (null == $client = $this->model->find($id)) {
            throw new ModelNotFoundException("client not found");
        }

        return $client;
    }

    // Your PHP installation needs cUrl support, which not all PHP installations
// include by default.
// To run under docker:
// docker run -v $PWD:/code php:7.3.2-alpine php /code/code_sample.php

    public function smsCheck()
    {
        $username = 'jimbo7';
        $password = 'Jmedina7.21';
        $messages = array(
          array('to'=>'+34666921860', 'body'=>'!Prueba cointrato desde el sistema')
        );

        $result = $this->send_message( json_encode($messages), 'https://api.bulksms.com/v1/messages?auto-unicode=true&longMessageMaxParts=30', $username, $password );

        if ($result['http_status'] != 201) {
          print "Error sending: " . ($result['error'] ? $result['error'] : "HTTP status ".$result['http_status']."; Response was " .$result['server_response']);
        } else {
          print "Response " . $result['server_response'];
          // Use json_decode($result['server_response']) to work with the response further
        }
    }

    public function smsCheckCode($client)
    {
        $username = 'guillermo246';
        $password = 'Nouguesa77';
        $messages = array(
          array('from' => 'RegData', 'to'=> $client->phone, 'body'=>'!Prueba cointrato desde el sistema. Codigo : '. $client->sms_confirm_token )
        );

        $result = $this->sendMessage( json_encode($messages), 'https://api.bulksms.com/v1/messages?auto-unicode=true&longMessageMaxParts=30', $username, $password );

        if ($result['http_status'] != 201) {
          return  ["error" => "Error sending: " . ($result['error'] ? $result['error'] : "HTTP status ".$result['http_status']."; Response was " .$result['server_response'])];
        } else {
          return $result;
          // Use json_decode($result['server_response']) to work with the response further
        }
    }

    public function sendMessage ( $post_body, $url, $username, $password)
    {
        $ch = curl_init( );
        $headers = array(
        'Content-Type:application/json',
        'Authorization:Basic '. base64_encode("$username:$password")
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, 1 );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_body );
        // Allow cUrl functions 20 seconds to execute
        curl_setopt ( $ch, CURLOPT_TIMEOUT, 20 );
        // Wait 10 seconds while trying to connect
        curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 10 );
        $output = array();
        $output['server_response'] = curl_exec( $ch );
        $curl_info = curl_getinfo( $ch );
        $output['http_status'] = $curl_info[ 'http_code' ];
        $output['error'] = curl_error($ch);
        curl_close( $ch );
        return $output;
    }
}
