<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    protected $fillable = [
        'user_id','name', 'username','email','phone','wallet_num',
        'active','dni', 'discount','type_id','id_f','id_b','birthdate',
        'term_selfie','auth_image'
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function client()
    {
        return $this->hasMany(Client::class);
    }

    public function AgentUpdate()
    {
        return $this->hasMany(Agent::class, 'agent_id', 'id');
    }

    public function sale()
    {
        return $this->hasMany(Sale::class);
    }

}
