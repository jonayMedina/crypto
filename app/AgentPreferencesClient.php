<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentPreferencesClient extends Model
{
  protected $fillable = [
    'agent_id','header_title', 'header_description','sb_left','instagram','facebook','twitter','sb_right', 'agent_name','agent_user','agent_web','body_img'
  ];

  public function agent()
  {
    return $this->belongsTo(Agent::class, 'agent_id', 'id');
  }

  protected $casts = [
    'created_at' => 'datetime:H:i:s d/m/Y', // Change format
    'updated_at' => 'datetime:H:i:s d/m/Y',
  ];
}
