<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Percent extends Model
{
    protected $fillable = [
        'amount', 'type', 'active', 'currency_id', 'description'
    ];

    protected $casts = [
	    'created_at' => 'datetime:H:i:s d/m/Y ', // Change format
	    'updated_at' => 'datetime:H:i:s d/m/Y',
	];

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id', 'id');
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

}
