<?php

namespace App\Http\Middleware;

use Closure;

class AgentTools
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if (! $request->user()->isAgent()) {

        return response()->json(['error'=>'Unauthorized-jwt'], 401);
      }
      return $next($request);
    }
}
