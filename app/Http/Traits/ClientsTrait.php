<?php

namespace App\Http\Traits;

use DB;
use Image;
use App\Client;
use Carbon\Carbon;
use App\Http\Traits\HelpersTrait;
use App\Http\Requests\StoreClient;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

trait ClientsTrait
{
    use HelpersTrait;

    public function clientStore($request)
    {
      $img = 0;
      try {

          DB::beginTransaction();
          $client = new Client([
              'agent_id' => $request->agent_id,
              'name' => strtoupper($this->deleteAccent($request->name)),
              'last_name' => strtoupper($this->deleteAccent($request->last_name)),
              'type_id' => $request->type_id,
              'dni' => $request->dni,
              'address' => $request->address,
              'expiration_date_dni' => $request->expiration_date_dni,
              'phone' => $request->phone_code.ltrim($request->phone, '0'),
              'notes' => $request->notes,
              'email' => $request->email,
              'birthdate' => $request->birthdate,
              'gender' => $request->gender,
              'nation_id' => $request->nation_id,
              'country_id' => $request->country_id,
              'terms_accepted' => $request->terms_accepted,
              'active' => 1,
              'approved' => 0,
          ]);

          $client->save();

          if ($request->hasFile('id_f')) {
              $image = $request->file()['id_f'];
              $url = $this->saveImage($image,'clients/'.$client->id,$client->id);
              $img++;
          }else { $url=null;}

          if ($request->hasFile('id_b')) {
              $image = $request->file()['id_b'];
              $url2 = $this->saveImage($image,'clients/'.$client->id,$client->id);
              $img++;
          }else { $url2=null;}

          if ($request->hasFile('term_selfie')) {
              $image = $request->file()['term_selfie'];
              $url3 = $this->saveImage($image,'clients/'.$client->id,$client->id);
              $img++;
          } else { $url3=null;}

          if ($request->hasFile('auth_image')) {
              $image = $request->file()['auth_image'];
              $url4 = $this->saveImage($image,'clients/'.$client->id,$client->id);
              $img++;
          }else { $url4=null;}

          $client->id_f = $url;
          $client->id_b = $url2;
          $client->term_selfie = $url3;
          $client->auth_image = $url4;
          if ($img == 3 || $img ==4) {
              $client->approved=1;
          }
          $client->save();
          DB::commit();

          return ['client' => $client];

      } catch (Exception $e) {
          DB::rollBack();
          return ['error' => $e];
      }
    }

    public function deleteAccent($string)
    {
      //Reemplazamos la A y a
      $string = str_replace(
        array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
        array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
      $string );

      //Reemplazamos la E y e
      $string = str_replace(
        array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
        array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
      $string );

      //Reemplazamos la I y i
      $string = str_replace(
        array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
        array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
      $string );

      //Reemplazamos la O y o
      $string = str_replace(
        array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
        array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
      $string );

      //Reemplazamos la U y u
      $string = str_replace(
        array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
        array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
      $string );

      //Reemplazamos la N, n, C y c
      $string = str_replace(
        array('Ç', 'ç'),
        array('C', 'c'),
      $string );

      return $string;
	  }
}
