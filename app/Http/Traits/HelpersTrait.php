<?php

namespace App\Http\Traits;

use App\Sale;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Image;

trait HelpersTrait
{
    public function saveImage($image, $path, $id)
    {
      if ($image->width() > 1000) {
        $image_original = $image->resize(1000,null,function ($constraint) {
          $constraint->aspectRatio();
          $constraint->upsize();
        });
      } else {
          $image_original = $image;
      }

      $date = Carbon::now();
      $extension = $aimge->extension();
      $name = $id. '-'. $date->format('d-m-Y-H-m-s');
      $url = $image->storeAs($path, $name .".".$extension);
      return $url;

    }

    public function pagesLimits($max)
    {
      $limits = [];

      $sale = Sale::select('id')->orderBy('id', 'desc')->first();
      $count = $sale->id;
      if ($count > $max) {
        $pages = $count/$max;
        $pages=ceil($pages);
      } else {
        $pages = 1;
      }

      for ($i=0; $i <($pages) ; $i++) {
        if ($i ==0) {
          $limits[$i]['first'] = 0;
          $limits[$i]['last'] = $max;
        } else {
          $limits[$i]['first'] = $limits[$i-1]['first']+$max;
          $limits[$i]['last'] = $limits[$i-1]['last']+$max;
        }
      }
      return $limits;
    }

    public function deleteAccent($string)
    {
      //Reemplazamos la A y a
      $string = str_replace(
        array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
        array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
      $string );

      //Reemplazamos la E y e
      $string = str_replace(
        array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
        array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
      $string );

      //Reemplazamos la I y i
      $string = str_replace(
        array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
        array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
      $string );

      //Reemplazamos la O y o
      $string = str_replace(
        array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
        array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
      $string );

      //Reemplazamos la U y u
      $string = str_replace(
        array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
        array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
      $string );

      //Reemplazamos la N, n, C y c
      $string = str_replace(
        array('Ç', 'ç'),
        array('C', 'c'),
      $string );

      return $string;
    }

    public function getIp()
    {
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }
                }
            }
        }
    }
}
