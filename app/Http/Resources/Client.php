<?php

namespace Resources;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Resources\Json\JsonResource;

class Client extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => ucfirst($this->name),
            'last_name' =>($this->last_name) ? ucfirst($this->last_name) : '',
            'full_name' =>($this->last_name) ? ucfirst($this->name. ' '. $this->last_name): ucfirst($this->name),
            'email' => $this->email,
            'phone' => $this->phone,
            'notes' => $this->notes,
            'agent_username' => ($this->agent) ? ucfirst($this->agent->username) : '',
            'agent_id' => ($this->agent) ? ucfirst($this->agent->id) : '',
            'active' => $this->active,
            'gender' => $this->gender,
            'type_id' => $this->type_id,
            'approved' => $this->approved,
            'birthdate' => $this->birthdate,
            'accumulated' => $this->accumulated,
            'country' => ($this->country_id) ? $this->country->name: '',
            'native' => ($this->native_id) ? $this->native->name: '',
            'dni' =>$this->dni,
            'expiration_date_dni' =>$this->expiration_date_dni,
            'address' =>$this->address,
            'id_f' => ($this->id_f) ? Storage::url($this->id_f): '',
            'id_b' => ($this->id_b) ? Storage::url($this->id_b): '',
            'term_selfie' => ($this->term_selfie) ? Storage::url($this->term_selfie): '',
            'auth_image' => ($this->auth_image) ? Storage::url($this->auth_image): '',
            'sms_verified_at'=> ($this->sms_verified_at) ? $this->sms_verified_at : '',
            'created_at' =>$this->created_at->format('H:i:s d/m/Y'),
            'updated_at' =>$this->updated_at->format('H:i:s d/m/Y'),
        ];
    }
}
