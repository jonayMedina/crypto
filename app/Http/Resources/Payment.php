<?php

namespace App\Http\Resources;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Resources\Json\JsonResource;

class Payment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->agency_num=='undefined') {
            $agency_num = ' ';
        } else {
            $agency_num = $this->agency_num;
        }
        return [
            'id' => $this->id,
            'file' => ($this->file) ? Storage::url($this->file): '',
            'bank_id' => $this->bank_id,
            'bank' => $this->bank,
            'agent' => $this->agent,
            'client' => $this->client,
            'client_id' => $this->client_id,
            'description' => $this->description,
            'agency_num' => $agency_num,
            'operation_method' => $this->operation_method,
            'iso' => ($this->iso) ? $this->iso: '',
            'active' =>$this->active,
            'price_btc' => $this->price_btc,
            'total_btc' => $this->total_btc,
            'amount' => $this->amount,
            'created_at' =>$this->created_at->format('H:i:s d/m/Y'),
            'amount_formated' => number_format($this->amount,2,',','.'),
            'price_formated' => number_format($this->price_btc,2,',','.'),
            'total_formated' => number_format($this->total_btc,8,',','.'),
        ];
    }
}
