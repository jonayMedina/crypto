<?php

namespace App\Http\Controllers;

use DB;
use PDF;
use App\Sale;
use App\Client;
use App\Balance;
use App\Http\Resources\PaymentCollection;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\Payment as PaymentResource;
use App\Http\Requests\StoreSale;
use App\Http\Traits\SalesTrait;
use App\Http\Traits\HelpersTrait;
use Illuminate\Support\Facades\Auth;

class PayController extends Controller
{
    use HelpersTrait, SalesTrait;

    public function __construct()
    {
        $this->middleware('agent');
    }

    public function agentCount($id)
    {
        if (Auth::user()->agent_id != $id) {
          return response()->json(['error'=>'Unauthorized-jwt'], 401);
        }
        $total = [];
        $amount = 0;
        $date = Carbon::now();
        $day = $date->day;
        $month = $date->month;
        $sales = Sale::where('agent_id',$id)->whereMonth('created_at', '>=', $month)->get();

        $count = $sales->count();
        for ($d=1; $d <= $day; $d++) {
            for ($i=0; $i <$count ; $i++) {
                if (Carbon::parse($sales[$i]->payment_date)->day == $d) {
                    $amount += $sales[$i]->amount;
                }
            }
            $days[$d] = $d;
            $total[$d] = $amount;
            $amount = 0;
        }
        $count = $sales->count();
        return response()->json(['count' => $count, 'total'=>$total, 'days' => $days]);
    }

    public function edit($id)
    {
        $sale = Sale::with('client:id,name,dni,phone,accumulated')->find($id);
        return response()->json($sale);
    }

    public function store(StoreSale $request)
    {
      $validated = $request->validated();

      $response = $this->saleTraitStore($request);
      return $response;
    }

    public function indexAgent($id)
    {
        if (Auth::user()->agent_id != $id) {
          return response()->json(['error'=>'Unauthorized-jwt'], 401);
        }
        $pay = new PaymentCollection(Sale::where('agent_id',$id)->with(['agent:id,username', 'client:id,name,last_name', 'bank:id,name'])->get()->reverse());

        return response()->json(['sales'=> $pay]);
    }

    public function update(Request $request,$id)
    {
        $sale = Sale::findOrFail($id);
        if (Auth::user()->agent_id != $sale->agent_id) {
          return response()->json(['error'=>'Unauthorized-jwt'], 401);
        }
        $client = Client::where('id', $sale->client_id)->first();
        if ($sale->iso == 'EUR' ) {
          if ($sale->active == 1 || $sale->active == 3) {
            if ($request->iso == 'EUR') {
              if ($request->client_id != $sale->client_id) {
                  $client->accumulated = $client->accumulated - $sale->amount;
                  $client->save();

                  $client = Client::where('id', $request->client_id)->first();
                  $client->accumulated = $client->accumulated + $request->amount;
                  $client->save();
              } else{
                  $client->accumulated = ($client->accumulated - $sale->amount) + $request->amount;
                  $client->save();
              }

            } else{
              $client->accumulated = $client->accumulated - $sale->amount;
              $client->save();
            }

          }
        }


        $sale->update([
          'bank_id' => $request->bank_id,
          'client_id' => $request->client_id,
          'description' => $request->description,
          'total_btc' => $request->total_btc,
          'iso' => $request->iso,
          'operation_method' => $request->operation_method,
          'amount' => $request->amount,
          'agency_num' => $request->agency_num,
          'user_ip' => $this->getIp(),
          'code' => $request->code,
        ]);

        $sale->save();

        return response()->json(['message' => 'Pago Actualizado']);
    }

    public function destroy($id)
    {
        $sale = Sale::findOrFail($id);
        if (Auth::user()->agent_id != $sale->agent_id) {
          return response()->json(['error'=>'Unauthorized-jwt'], 401);
        }
        if ($sale->active != 1 || $sale->active != 3) {
          $sale->delete();
          return response()->json(['message' => 'La venta ha sido Eliminada']);
        } else {
          return response()->json(['error' => 'No autorizado'], 402);
        }
    }

    public function imgDownload($id)
    {
        $sale = Sale::findOrFail($id);
        if (Auth::user()->agent_id != $sale->agent_id) {
          return response()->json(['error'=>'Unauthorized-jwt'], 401);
        }
        return Storage::download($sale->file);
    }
}
