<?php

namespace App\Http\Controllers;

use DB;
use App\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;

class SenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $agent = Auth::user();
         $clients = Client::where('agent_id', $agent->id)->all();
        // dd($clients);
        return response()->json($clients);
    }

    public function agentClients($id)
    {
        $clients = Client::where('agent_id',$id)->where('active',1)->get();
        return response()->json($clients);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $client = new Client([
            'agent_id' => $request->agent_id,
            'name' => $request->name,
            'dni' => $request->dni,
            'notes' => $request->notes,
            'active' => 1,
        ]);
        $client->save();

        return response()->json('Guardada');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sale  $client
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Client::find($id);
        return response()->json($client);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sale  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Sale $client)
    {
        $client = Client::find($id);
        return response()->json($client);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sale  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $client = Client::findOrFail($id);
        $client->update($request->all());

        return response()->json('actualizado');
    }

    public function desactive(Request $request)
    {
        $client = Client::find($request->id);
        $client->active = 0;
        $client->save();

        return response()->json('desactivado');
    }

    public function activate(Request $request)
    {
        $client = Client::find($request->id);
        $client->active = 1;
        $client->save();

        return response()->json('activado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sale  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $client = Client::findOrFail($id)->delete();

        return response()->json('Eliminado');
    }
}
