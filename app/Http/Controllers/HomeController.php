<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Country;
use App\Client;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('pages.index');
    }

    public function about()
    {
        return view('pages.about');
    }

    public function contact()
    {
        return view('pages.contact');
    }

    // public function rate()
    // {
    //     return view('pages.rate');
    // }

    public function services()
    {
        return view('pages.services');
    }

    public function policy()
    {
        return view('pages.policy');
    }

    public function terms()
    {
        return view('pages.terms');
    }

    public function amlKyc()
    {
        return view('pages.aml-kyc');
    }

    public function admin()
    {
        return view('admin.index');
    }

    public function clientRegister($username)
    {
      $countries = Country::active()->get();
      $agent = Agent::whereUsername($username)->first();
      return view('clients.register')->with(['agent' => $agent, 'countries' => $countries]);
    }

    public function clientVerifySms(Client $client)
    {

      return view('clients.verify-sms')->with(['client' => $client]);
    }


  }
