<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use App\Agent;
use App\AgentUpdate;
use App\Sale;
use App\Country;
use App\Client;    
use Illuminate\Http\Request;
use App\Http\Traits\HelpersTrait;

class AgentController extends Controller
{
    use HelpersTrait;

    public function index()
    {
        $agents = Agent::orderBy('id', 'desc')->get();
        return response()->json(['agents' => $agents]);
    }

    public function agentsActive()
    {
        $agents = Agent::where('active', 1)->orderBy('id', 'desc')->get();
        return response()->json(['agents' => $agents]);
    }

    public function store(Request $request)
    {

        $agent = new Agent([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'wallet_num' => $request->wallet_num,
            'username' => $request->username,
            'discount' => $request->discount,
            'active' => 1,
        ]);
        $agent->save();

        $agentUpdate = new AgentUpdate([
          'user_id' => Auth::user()->id,
          'name' => $request->name,
          'email' => $request->email,
          'phone' => $request->phone,
          'wallet_num' => $request->wallet_num,
          'username' => $request->username,
          'discount' => $request->discount,
          'active' => 1,
          'user_ip' => $this->getIp(),
        ]);
        $agentUpdate->save();

        return response()->json(['message' => $agent->name .' Guardado']);
    }

    public function agentRegister(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        try {
            DB::beginTransaction();
            $user = new User();
            $user->name = ucwords(mb_strtolower($request->name));
            $user->email = strtolower($request->email);
            $user->password = bcrypt($request->password);
            $user->active = 1;
            $user->role = 1;
            $user->save();

            $agent = new Agent();
            $agent->user_id = $user->id;
            $agent->name = $user->name;
            $agent->email = $user->email;
            $agent->phone = $request->phone;
            $agent->username = $request->username;
            $agent->wallet_num = $request->wallet_num;
            $agent->type_id = $request->type_id;
            $agent->dni = $request->dni;
            $agent->active = 1;
            $agent->save();

            $user->agent_id = $agent->id;
            $user->save();

            $agentUpdate = AgentUpdate::create([
              'user_id' => Auth::user()->id,
              'agent_id' => $agent->id,
              'name' => $request->name,
              'email' => $request->email,
              'phone' => $request->phone,
              'wallet_num' => $request->wallet_num,
              'username' => $request->username,
              'dni' => $request->dni,
              'discount' => $agent->discount,
              'user_ip' => $this->getIp(),
              'active' => 1,
            ]);


            DB::commit();
            return response()->json(['message'=> $agent->name. ' Registrado '], 200);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error'=>$e], 400);
        }
    }

    public function show($id)
    {
        $agent = Agent::find($id);
        return response()->json($agent);
    }

    public function get($id)
    {
        $agent = Agent::where('user_id',$id)->first();
        return response()->json($agent);
    }

    public function agentClients()
    {
        $clients_all = [];
        $id = Auth::user()->agent_id;
        $clients = Client::where('agent_id', $id)->get();
        if ($clients) {
            array_push($clients_all, $clients);
        }
        $sales = Sale::where('agent_id',$id)->get();
        foreach ($sales as $key => $sale) {
            $clients_all[] = $sale->client;
        }
        $clients_all = array_values(array_filter(array_unique($clients_all)));
        return response()->json(['clients' => $clients_all]);
    }

    public function edit($id)
    {
        $agent = Agent::find($id);
        return response()->json(['agent' => $agent]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sale  $agent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $agent = Agent::findOrFail($id);
        $user = User::where('agent_id', $id)->first();
        if (Auth::user()->role !=3) {
          if (Auth::user()->agent_id != $agent->id) {
            return response()->json(['error'=>'Unauthorized-jwt'], 401);
          }
        }
        try {
            if (!$user) {
                $user = new User();
                $user->name = $request->name;
                $user->agent_id = $agent->id;
                $user->email = $request->email;
                $user->password = bcrypt($request->password);
                $user->active = 1;
                $user->role = 1;
                $user->save();

                $agent->user_id = $user->id;
                $agent->name = $user->name;
                $agent->email = $user->email;
                $agent->phone = $request->phone;
                $agent->username = $request->username;
                $agent->discount = $request->discount;
                $agent->wallet_num = $request->wallet_num;
                $agent->type_id = $request->type_id;
                $agent->dni = $request->dni;
                $agent->save();
                AgentUpdate::create([
                  'user_id' => Auth::user()->id,
                  'name' => $request->name,
                  'email' => $request->email,
                  'phone' => $request->phone,
                  'wallet_num' => $request->wallet_num,
                  'username' => $request->username,
                  'discount' => $request->discount,
                  'dni' => $request->dni,
                  'old_name' => $agent->name,
                  'old_email' => $agent->email,
                  'old_phone' => $agent->phone,
                  'old_wallet_num' => $agent->wallet_num,
                  'old_username' => $agent->username,
                  'old_dni' => $agent->dni,
                  'user_ip' => $this->getIp(),
                  'active' => 1,
                ]);

            } else{
                $agent->user_id = $user->id;
                $agent->name = $user->name;
                $agent->email = $user->email;
                $agent->phone = $request->phone;
                $agent->username = $request->username;
                $agent->discount = $request->discount;
                $agent->wallet_num = $request->wallet_num;
                $agent->type_id = $request->type_id;
                $agent->dni = $request->dni;
                $agent->save();

                AgentUpdate::create([
                  'user_id' => Auth::user()->id,
                  'agent_id' => $agent->id,
                  'name' => $request->name,
                  'email' => $request->email,
                  'phone' => $request->phone,
                  'wallet_num' => $request->wallet_num,
                  'username' => $request->username,
                  'discount' => $request->discount,
                  'old_name' => $agent->name,
                  'old_email' => $agent->email,
                  'old_phone' => $agent->phone,
                  'old_wallet_num' => $agent->wallet_num,
                  'old_username' => $agent->username,
                  'old_dni' => $agent->dni,
                  'user_ip' => $this->getIp(),
                  'active' => 1,
                ]);

                $user->update($request->all());
                $user->save();
            }


            DB::commit();
            return response()->json(['message'=> $agent->name. ' Actualizado!'], 200);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error'=>$e], 400);
        }
    }

    public function newPassword(Request $request,$id)
    {
        $agent = Agent::findOrFail($id);

        if (!$agent->email) {
            return response()->json(['email'=>'Por Favor Editar Agente y Agregar Email'],400);
        }

        $user = User::where('agent_id', $id)->first();
        try {
            if (!$user) {
                $user = new User();
                $user->name = $agent->name;
                $user->agent_id = $agent->id;
                $user->email = $agent->email;
                $user->password = bcrypt($request->password);
                $user->active = 1;
                $user->role = 1;
                $user->save();

                $agent->user_id = $user->id;
                $agent->save();
            } else {
                $user->password = bcrypt($request->password);
                $user->save();
            }
            DB::commit();
            return response()->json(['message'=> $agent->name. ' Actualizado '], 200);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error'=>$e], 400);
        }


    }

    public function desactive($id)
    {
        $agent = Agent::find($id);
        $agent->active = 0;
        $agent->save();

        return response()->json(['message'=>'Agente desactivado']);
    }

    public function activate($id)
    {
        $agent = Agent::find($id);
        $agent->active = 1;
        $agent->save();

        return response()->json(['message'=>'Agente activado']);
    }

    public function destroy($id)
    {
        $agent = Agent::findOrFail($id);
        $user = User::where('agent_id', $id)->first();
        $agent->delete();
        if ($user) {
            $user->delete();
        }

        return response()->json(['message'=>'Agente Eliminado']);
    }

    public function email(Request $request)
    {
        $email = Agent::where('email', $request->email)->first();
        return response()->json(['email'=>$email]);
    }

    public function user(Request $request)
    {
        $user = Agent::where('username', $request->user)->first();
        return response()->json(['user'=>$user]);
    }
}
