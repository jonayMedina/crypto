<?php

namespace App\Http\Controllers;

use App\Balance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BalanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $last = Balance::active()->first();
        $balances = Balance::inactive()->orderBy('id', 'desc')->get();
        return response()->json(['balances' => $balances, 'last' => $last], 200);
    }

    public function active()
    {
        $balance = Balance::active()->first();
        return response()->json(['balance' => $balance], 200);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'amount' => "required"
        ]);
        if($validator->fails()){
            return response()->json($validator->messages(), 403);
        }

        $balance = Balance::create([
            'web' => $request->web,
            'amount' => $request->amount,
            'symbol' => $request->symbol,
            'type' => 'Inicial',
            'active' => 1
        ]);

        return response()->json(['message' => 'Guardado'], 200);
    }

    protected function storeTranssaction($web = null, $amount, $operation, $before, $symbol = null, $type, $notes =null)
    {
      $sum = 0.0;
      $sub = 0.0;

      if($type == 'Aumento'){
        $sum = round($operation, 8);
      } elseif ($type == 'Disminución') {
        $sub = round($operation, 8);
      }

      $balance_new = Balance::create([
        'web' => $web,
        'amount' => round($amount, 8),
        'amount_operation' => round($operation, 8),
        'amount_sum_operation' => $sum,
        'amount_minus_operation' => $sub,
        'amount_before_operation' => round($before, 8),
        'symbol' => $symbol,
        'type' => $type,
        'notes' => $notes,
        'active' => 0
      ]);

      return $balance_new;
    }

    public function show(Balance $balance)
    {
        //
    }

    public function modif(Request $request, Balance $balance)
    {
        $validator = Validator::make($request->all(),[
            'amount' => "required"
        ]);
        if($validator->fails()){
            return response()->json($validator->messages(), 403);
        }

        $before = $balance->amount;
        $balance->update([
            'web' => $request->web,
            'amount' => $request->amount,
            'amount_operation' => $request->amount,
            'amount_before_operation' => $before,
            'type' => 'Modificación',
            'symbol' => $request->symbol
        ]);
        $balance->save();

        $this->storeTranssaction($request->web, $request->amount, $request->amount, $before, $request->symbol, 'Modificación', $request->notes);

        return response()->json(['message' => 'Actualizado'], 200);
    }

    public function update(Request $request, Balance $balance)
    {
        $validator = Validator::make($request->all(),[
            'amount' => "required"
        ]);
        if($validator->fails()){
            return response()->json($validator->messages(), 403);
        }

        $before = $balance->amount;

        $balanceActive = Balance::where('active',1)->first();
        if ($balance->amount_sum_operation) {
          $balanceActive->amount = $request->amount + ($balanceActive->amount - $balance->amount_sum_operation);
        } elseif ($balance->amount_minus_operation) {
          $balanceActive->amount = $request->amount - ($balanceActive->amount + $balance->amount_minus_operation);
        } elseif ($balance->type == 'Modificación') {
          $balanceActive->amount = $balance->before;
        }
        $balance->update([
            'web' => $request->web,
            'amount' => $request->amount,
            'amount_operation' => $request->amount,
            'amount_before_operation' => $before,
            'type' => 'Modificación',
            'symbol' => $request->symbol
        ]);
        $balance->save();

        $this->storeTranssaction($request->web, $request->amount, $request->amount, $before, $request->symbol, 'Modificación', $request->notes);

        return response()->json(['message' => 'Actualizado'], 200);
    }

    public function addBtc(Request $request, Balance $balance)
    {
        $validator = Validator::make($request->all(),[
          'add_btc' => "required"
        ]);

        if($validator->fails()){
          return response()->json($validator->messages(), 403);
        }
        if ($request->sub == '+') {
          $amount = $balance->amount + $request->add_btc;
          $type = 'Aumento';
        } elseif ($request->sub == '-') {
          $amount = $balance->amount - $request->add_btc;
          $type = 'Disminución';
        }
        $before = $balance->amount;
        $balance->update([
            'amount' => round($amount, 8)
        ]);

        $balance_new = $this->storeTranssaction($balance->web, $amount, $request->add_btc, $before, $balance->symbol, $type, $request->notes);

        return response()->json(['message' => 'Actualizado', 'balance' => $balance, 'balance_new' => $balance_new], 200);
    }

    public function destroy(Balance $balance)
    {
        if ($balance->active == 1) {
            return response()->json(['error'=> 'Activar otro balance Antes de eliminar'],405);
        }
        $balanceActive = Balance::where('active',1)->first();
        if ($balance->amount_sum_operation) {
          $balanceActive->amount = $balanceActive->amount - $balance->amount_sum_operation;
        } elseif ($balance->amount_minus_operation) {
          $balanceActive->amount = $balanceActive->amount + $balance->amount_minus_operation;
        } elseif ($balance->type == 'Modificación') {
          $balanceActive->amount = $balance->before;
        }

        $balance->delete();
        return response()->json(['message'=>'Deleted'],204);
    }

    public function activate(Balance $balance)
    {
        $balanceActive = Balance::where('active',1)->first();

        $percent->update(['active' => 1]);
        if ($balanceActive) {
            $balanceActive->update(['active' => 0]);
        }

        return response()->json(['message' =>'Exito! Cambiado'],200);
    }
}
