<?php

namespace App\Http\Controllers;

use App\Percent;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PercentController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $percents = Percent::with('currency:id,iso')->orderBy('active', 'desc')->get();
        return response()->json(['percents' => $percents]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'amount' => 'required|numeric',
        ]);
        if($validator->fails()){
            return response()->json($validator->messages(), 403);
        }

        $percent = new Percent ([
            'amount' => $request->amount,
            'type' => ucwords(mb_strtolower($request->type)),
            'currency_id' => $request->currency_id,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        $percent->save();

        return response()->json(['message' => 'Saved'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Percent  $percent
     * @return \Illuminate\Http\Response
     */
    public function getActive()
    {
        $percent = Percent::where('active',1)->get();

        return response()->json(['percent' =>$percent], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Percent  $percent
     * @return \Illuminate\Http\Response
     */
    public function edit(Percent $percent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Percent  $percent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'amount' => 'required|numeric'
        ]);
        if($validator->fails()){
            return response()->json($validator->messages(), 403);
        }

        Percent::where('id',$request->id)->update([
            'amount' => $request->amount,
            'type' => ucwords(mb_strtolower($request->type)),
            'currency_id' => $request->currency_id,
            'updated_at' => Carbon::now(),
        ]);

        return response()->json(['message' => 'Porcentaje Actualizado'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Percent  $percent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        $percent = Percent::findOrFail($request->id);
        if ($percent->active == 1) {
            return response()->json(['error'=> 'Activar otro porcentaje Antes de eliminar'],405);
        }

        $percent->delete();
        return response()->json(['message'=>'Deleted'],204);
    }

    public function activate(Percent $percent)
    {
        $percentActive = Percent::where('currency_id', $percent->currency_id)->where('active',1)->first();

        $percent->update(['active' => 1]);
        if ($percentActive) {
            $percentActive->update(['active' => 0]);
        }

        return response()->json(['message' =>'Exito! Cambiado'],200);
    }
}
