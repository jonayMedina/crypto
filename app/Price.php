<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $fillable = ['amount', 'notes',  'active', 'currency_id'];

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id', 'id');
    }

    protected $casts = [
	    'created_at' => 'datetime:H:i:s d/m/Y ', // Change format
	    'updated_at' => 'datetime:H:i:s d/m/Y',
	];
}
