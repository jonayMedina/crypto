<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
  protected $fillable = ['iso', 'name'];

  // Relations
  public function clients()
  {
      return $this->hasMany(Client::class, 'country_id', 'id');
  }

  // ModelScopes
  public function scopeActive($query)
  {
      return $query->where('active',1);
  }
  public function scopeByName($query)
  {
      return $query->orderBy('name','ASC');
  }
}
