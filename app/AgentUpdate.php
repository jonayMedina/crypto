<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentUpdate extends Model
{
  protected $fillable = [
    'agent_id','name', 'username','email','phone','wallet_num','active','dni', 'discount','type_id','id_f','id_b','birthdate','term_selfie','auth_image', 'user_id',  'old_name', 'old_email',
    'old_phone', 'old_wallet_num', 'old_username', 'old_dni'
  ];

  public function agent()
  {
    return $this->belongsTo(Agent::class, 'agent_id', 'id');
  }

  public function user()
  {
    return $this->belongsTo(User::class, 'user_id', 'id');
  }

  protected $casts = [
    'created_at' => 'datetime:H:i:s d/m/Y', // Change format
    'updated_at' => 'datetime:H:i:s d/m/Y',
  ];
}
