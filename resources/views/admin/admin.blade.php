<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <script src="{{ asset('./js/app.js') }}" defer></script>
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
    <!-- Fonts -->
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@5.x/css/materialdesignicons.min.css" rel="stylesheet">
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('./css/app.css') }}" rel="stylesheet">

    <style>

        .preloader {
            background-color: #f7f7f7;
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            z-index: 999999;
            -webkit-transition: .6s;
            -o-transition: .6s;
            transition: .6s;
            margin: 0 auto;
        }

        /* line 611, Internet_service/assets/scss/_common.scss */
        .preloader .preloader-circle {
            width: 110px;
            height: 110px;
            position: relative;
            border-style: solid;
            border-width: 2px;
            border-top-color: #0f132d;
            border-bottom-color: transparent;
            border-left-color: transparent;
            border-right-color: transparent;
            z-index: 10;
            border-radius: 50%;
            -webkit-box-shadow: 0 2px 10px 0 rgba(35, 181, 185, 0.15);
            box-shadow: 0 1px 5px 0 rgba(35, 181, 185, 0.15);
            background-color: #fff;
            -webkit-animation: zoom 2000ms infinite ease;
            animation: zoom 2000ms infinite ease;
            -webkit-transition: .6s;
            -o-transition: .6s;
            transition: .6s;
        }

        /* line 633, Internet_service/assets/scss/_common.scss */
        .preloader .preloader-circle2 {
            border-top-color: #0078ff;
        }

        /* line 636, Internet_service/assets/scss/_common.scss */
        .preloader .preloader-img {
            position: absolute;
            top: 50%;
            z-index: 200;
            left: 0;
            right: 0;
            margin: 0 auto;
            text-align: center;
            display: inline-block;
            -webkit-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
            padding-top: 6px;
            -webkit-transition: .6s;
            -o-transition: .6s;
            transition: .6s;
        }

        /* line 654, Internet_service/assets/scss/_common.scss */
        .preloader .preloader-img img {
            max-width: 100px;
            border-radius: 50%;
        }

        /* line 657, Internet_service/assets/scss/_common.scss */
        .preloader .pere-text strong {
            font-weight: 800;
            color: #dca73a;
            text-transform: uppercase;
        }

        @-webkit-keyframes zoom {
            0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
            -webkit-transition: .6s;
            -o-transition: .6s;
            transition: .6s;
            }
            100% {
            -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
            -webkit-transition: .6s;
            -o-transition: .6s;
            transition: .6s;
            }
        }

        @keyframes zoom {
            0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
            -webkit-transition: .6s;
            -o-transition: .6s;
            transition: .6s;
            }
            100% {
            -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
            -webkit-transition: .6s;
            -o-transition: .6s;
            transition: .6s;
            }
        }
    </style>

</head>
<body>

    <div id="admin">
        <div id="preloader-active">
            <div class="preloader d-flex align-items-center justify-content-center">
                <div class="preloader-inner position-relative">
                    <div class="preloader-circle"></div>
                    <div class="preloader-img pere-text">
                        <img src="{{ asset('./img/logo.png') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
        <section id="loading">
            <div id="loading-content"></div>
        </section>
        <main-app></main-app>
    </div>
    <script>
        var laravel = @json(['baseURL' => url('/')])
    </script>
</body>
</html>
