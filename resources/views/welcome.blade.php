<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'Laravel') }}</title>
  <!-- Scripts -->
  <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
  <!-- Fonts -->
  <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
  <link rel="dns-prefetch" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="container mt-4">
<div class=" header center" id="app">
    <h5>Auto-reload page</h5>

    <div id="main" class="main">

    </div>
    <div id="pct"></div>



</div>
</body>

<script src="{{ asset('js/app.js') }}" defer></script>
<script>

    jQuery(document).ready(function() {
        // your code here
            getPrice();
        });
    function getPrice(){
        var $main = $('.main'),
        str = '',
        nodeNames = [];
        axios.get('./api/price/set')
            .then(function(response) {
                var pct = response.data.pct;
                var prices = response.data.prices;
                for (let i = 0; i < prices.length; i++) {
                    str += '<h5> Precio en : ' + prices[i]['iso']  +' precio calculado: '+ prices[i]['amount_calc'] + '<i> LocalBitcoin : ' + prices[i]['amount_lb'] +' </i> </h5>'
                }
                // console.log(str);
                $('#main').html(str);
                $('#pct').html('<h5> Porcentaje activo : ' + pct.amount + ' Dirigido ha : ' + pct.type + '</h5>');
            })
            .catch(function (error) {
                console.log(error);
            });
    }
    setInterval(getPrice, 60000);
    // setTimeout(getPrice, 1000);
</script>
</html>
