@extends('layouts.app-client')

@section('content')
<div class="container">
  <div class="card bg-light">
    <article class="card-body mx-auto" style="max-width: 70%;">
      <h4 class="card-title p-3 mb-2 bg-info text-white mt-3 text-center">Por Favor Verifica tu Telefono, Recibiras un codigo en pocos segundos al numero {{ $client->phone}}</h4>
      @if(Session::has('success'))
        <div class="alert alert-success">
            {{Session::get('success')}}
        </div>
      @endif

      <form class="form-row" method="post" action="{{ route('clients-check-sms', $client->id) }}">
        @csrf
        <input type="hidden" name="client_id" value="{{ $client->id}}">
        <div class="col-md-6">
          <div class="form-group input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"> <i class="fa fa-user"></i> </span>
            </div>
              <input id="code" name="code" class="form-control @error('code') is-invalid @enderror" placeholder="Ingrese Codigo de sms Recibido" type="text" value="{{ old('code') }}" required autocomplete="code" autofocus>

              @error('code')
                <div class="alert alert-danger">{{ $message }}</div>
              @enderror
          </div> <!-- form-group// -->
        </div>
        <div class="col-md-4 offset-md-4">
          @if($errors->any())
            <h4 class="text-dark">{{$errors->first()}}</h4>
          @endif
          <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block"> Verificar Telefono </button>
          </div>
        </div>
          <!-- form-group// -->
      </form>
    </article>
  </div> <!-- card.// -->
</div>

@endsection

@section('scripts')
  <script>
    $(document).ready(function($){
    });
  </script>
@endsection
