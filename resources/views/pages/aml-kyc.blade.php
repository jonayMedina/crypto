@extends('layouts.app-page')

@section('content')
    <!-- AML -->
    <div class="wallet">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12" style="min-height: 80vh">
                    <iframe width="100%" height="100%" src="{{asset('files/AML.pdf')}}" frameborder="0"></iframe>
                    {{-- <iframe width="100%" height="80vh" src="https://docs.google.com/viewer?url=http://unec.edu.az/application/uploads/2014/12/pdf-sample.pdf&embedded=true"  frameborder="0"></iframe> --}}
                </div>
            </div>
        </div>
    </div>
    <!-- end AML -->
@endsection
