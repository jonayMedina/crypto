<!DOCTYPE html>
<meta name="csrf-token" content="{{ csrf_token() }}">
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <title>CoinTrato</title>
    <meta name="keywords" content="btc, euros, EUR, compra btc, venta btc, remesas españa, remesas, enviar euro, enviar dinero">
    <meta name="description" content="Compra Venta BTC Euro Peso Colombiano">
    <meta name="author" content="jonay medina jonaymedinadev@gmail.com">
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
        <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <script src="{{ asset('js/jquery.min.js') }}"></script>
      <script src="{{ asset('js/custom.js') }}"></script>

    <!-- bootstrap css -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slicknav.css') }}">
    <!-- style css -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slick.css') }}">
    <!-- responsive-->
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
    <!-- awesome fontfamily -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


</head>
<!-- body -->

<body class="main-layout">
    <!-- loader  -->
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="{{ asset('./img/logo.png')}}" alt="">
                </div>
            </div>
        </div>
    </div>
    {{-- <div class="loader_bg">
        <div class="preloader-circle"></div>
        <div class="loader"><img src="{{ asset('./images/loading.gif') }}" alt="" /></div>
    </div> --}}
    <!-- end loader -->
    <!-- header -->
    <header>
        <!-- header inner -->
        <div class="head-top">
            <div class="container-fluid">
                <div class="row d_flex">
                    <div class="col-sm-3">
                        <div class="logo">
                            <a href="{{ route('index')}}">CoinTrato</a>
                            <img class="ml-1" src="{{ asset('./img/logo.png')}}" alt="">
                        </div>
                    </div>
                    <div class="col-xl-9 col-lg-9 text-right">
                            <!-- Main-menu -->
                            <div class="main-menu d-none d-lg-block">
                                <nav>
                                    <ul id="navigation">
                                        <li><a href="{{route('index')}}">Inicio</a></li>
                                        <li><a href="{{ route('rate')}}">Tasa</a></li>
                                        <li><a href="{{ route('about')}}">Nosotros</a></li>
                                        <li><a href="{{ route('services')}}">Servicios</a></li>
                                        <li><a href="{{ route('contact')}}">Contacto</a></li>
                                        <li ><a href="{{ url('/access/login')}}">Acceder <i class=" ml-2 fa fa-user" aria-hidden="true"></i></a></li>
                                    </ul>
                                </nav>
                            </div>
                    </div>
                    <div class="col-12 d-lg-none">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- end header -->

    @yield('content')
    <!--footer-->
      <footer>
        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="Informa conta ">
                            <h3>Contáctanos</h3>
                            <ul>
                                <li> <a href="Javascript:void(0) "> <i class="fa fa-map-marker " aria-hidden="true "></i> C/ Font Rotja 5, 25, 46007, Valencia, España.
                                </a>
                                </li>
                                <li><a href="https://api.whatsapp.com/send?phone=+34666921860" target="_blank"><i class="fa fa-whatsapp " aria-hidden="true "></i> +34666921860</a>
                                </li>
                                <li> <a href="https://msng.link/o/?@cointrato=tg"><i class="fa fa-telegram " aria-hidden="true "></i>Envianos en Telegram @cointrato
                                </a>
                                </li>
                                <li> <a href="mailto:info@cointrato.com"> <i class="fa fa-envelope " aria-hidden="true "></i> info@cointrato.com
                                </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 ">
                        <div class="Informa helpful">
                            <h3>Enlaces de Interés</h3>
                            <ul>
                                <li><a href="{{route('index')}}">Inicio</a></li>
                                <li><a href="{{ route('rate')}}">Tasa al momento</a></li>
                                <li><a href="{{ route('about')}}">Nosotros</a></li>
                                <li><a href="{{ route('services')}}">Servicios</a></li>
                                <li><a href="{{ route('contact')}}">Contacto</a></li>
                                <li ><a href="{{ url('/access/login')}}">Acceder <i class="fa fa-user" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6" >
                        <div class="Informa ">
                            <h3>Nuestros Valores</h3>
                            <ul>
                                <li>Honestidad y confianza plena.
                                </li>
                                <li>Diálogo directo, despejando dudas relacionadas.
                                </li>
                                <li>Ganas de enseñar y seguir aprendiendo.
                                </li>
                                <li>Ganas de innovar y crecer como personas.
                                </li>
                                <li>Política de verificación de personas.
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 ">
                        <div class="Informa helpful">
                            <h3>Área Legal</h3>
                            <ul>
                                <li><a href="{{ route('policy')}}">Políticas de Privacidad</a></li>
                                {{-- <li><a href="{{ route('terms')}}">Términos y Condiciones</a></li> --}}
                                <li><a href="{{ route('aml-kyc')}}">AML-KYC</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="copyright text_align_center ">
                    <div class="container ">
                        <div class="row ">
                            <div class="col-md-12 ">
                                <p>© 2020 All Rights Reserved. Design and Developed with <i class="fa fa-heart" aria-hidden="true"></i> by  <a style="color: darkgoldenrod" href="mailto:jonaymedinadev@gmail.com "> Jonay Medina</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </footer>
      <!-- end footer -->
      <!-- Javascript files-->

      <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
      <script src="{{ asset('js/jquery-3.0.0.min.js') }}"></script>

      <script src="{{ asset('js/jquery.slicknav.min.js') }}"></script>
      @yield('scripts')
    </body>
</html>
