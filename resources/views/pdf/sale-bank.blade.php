<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
<title>FACTURA N° {{ $sale->id}}</title>

    <style type="text/css">
        @page {
            margin: 0px;
        }
        body {
            margin: 0px;
        }
        * {
            font-family: Verdana, Arial, sans-serif;
        }
        a {
            color: #fff;
            text-decoration: none;
        }
        table {
            font-size: x-small;
        }
        tr {
            margin: 30px 0;
        }
        tfoot tr td {
            font-weight: bold;
        }
        .invoice table {
            margin: 15px;
        }
        .invoice h3 {
            margin-left: 15px;
        }
        .information {
            background-color: #60A7A6;
            color: #FFF;
        }
        .information .logo {
            margin: 5px;
        }
        .information table {
            padding: 10px;
        }
        .upper{
            text-transform: uppercase;
        }

        .information td, .information th {
          padding: 10px 15px;
          font-size: 15px;

        }
        .information td.header-rigth{
          padding-left:50px;
        }
        .invoice td, .invoice th {
          text-align: center;
          padding: 10px 15px;
          font-size: 15px;

        }
        .first-row{
          width: 50%;
          text-align: left !important;
        }
        div .divider {
          border-top: 3px solid #000;
        }
    </style>

</head>
<body>

<div class="information">
    <table width="100%">
        <tr>
            <td align="left" style="width: 40%;">
              <h3 class="upper">{{ $sale->bank->account_holder}}</h3>
              <h4><strong>NIF/CIF: </strong> {{ $sale->bank->dni_holder}}</h4>
              <h4> {{ $sale->bank->address}}</h4>
              <h4> {{ $sale->bank->phone_holder}}</h4>
              <h4> {{ $sale->bank->email}}</h4>

            </td>
            <td  style="">
                <img width="64" class="logo"/>
            </td>
            <td class="header-rigth" align="left" style="width: 40%;">
              <h4><strong>FACTURA N°: {{ $sale->id}}</strong></h4>
              <h4>FAC{{ $sale->id}}</h4>
              <h4><strong>FECHA:</strong> {{date("d m Y", strtotime($sale->created_at)) }}</h4>
              <h4><strong>SALDO DEUDOR:</strong></h4>
              <h4>EUR {{ $sale->amount}} €</h4>
            </td>
        </tr>
    </table>
</div>

<div class="invoice">
    <table width="100%">
      <tr>
        <td align="left" style="margin-left: 50px; width: 40%;">
          <h3>Cliente</h3>
          <h3 class="upper">{{ $sale->client->name}} {{ $sale->client->last_name}}</h3>
          <h3> Telef: {{ $sale->client->phone}}</h3>
          <h3> Email: {{ $sale->client->email}}</h3>
        </td>
        <td align="left" style="width: 40%">
          <h3>ID</h3>
          <h3>{{ $sale->client->dni}}</h3>
          <h3>Dirección:</h3>
          <h3>{{ $sale->client->address}}</h3>
        </td>
      </tr>
    </table>
    <div class="divider"></div>
    <table width="100%" >

      <thead>
        <tr aling="center">
            <th class="first-row">ARTÍCULO</th>
            <th>TARIFA</th>
            <th>CANT.</th>
            <th>TOTAL</th>
        </tr>
      </thead>
      <tbody>
        <tr aling="center">
            <td class="first-row" style="font-weight: bolder;">{{ $sale->total_btc }} BTC</td>
            <td>{{ $sale->amount}} €</td>
            <td >1</td>
            <td class="">{{ $sale->amount}} €</td>
        </tr>
      </tbody>

      <tfoot>
        <tr>
            <td colspan="2"></td>
            <td align="left">Total</td>
            <td align="rigth" class="gray">{{ $sale->amount}} €</td>
        </tr>
      </tfoot>
    </table>
</div>

<div class="information" style="position: absolute; bottom: 0;">
    <table width="100%">
        <tr>
            <td align="left" style="width: 50%;">
              <span style="color:red">Al comerciar con nosotros, acepta nuestras politicas de Privacidad y AML/KYC en <a href="https://cointrato.com">COINTRATO.COM</a></span>
              <span style="color:red">El comercio de Criptoactivos no están gravados por IVA. </span>
                {{-- &copy; {{ date('Y') }} {{ config('app.url') }} - All rights reserved. --}}
            </td>
            <td align="right" style="width: 50%;">
                {{-- Company Slogan --}}
            </td>
        </tr>

    </table>
</div>
</body>
</html>
