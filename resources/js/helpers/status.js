export default {
active : [
  {
    id: '0',
    text: 'LLEGA INMEDIATO',
    color:'orange'
  },
  {
    id: 2,
    text: 'NO LLEGA INMEDIATO',
    color: 'red'
  },
  {
    id:4,
    text:'NO LLEGA INMEDIATO "PAGAR AHORA"',
    color: 'purple'
  }
],

status : [
  {
    active:'0',
    text: 'En Revisión',
    color: 'orange'
  },
  {
    active:1,
    text: 'Aprobado y Pagado',
    color: 'green'
  },
  {
    active:2,
    text: 'Diferido en Espera',
    color: 'red'
  },
  {
    active:3,
    text: 'Pagado por Adelantado',
    color: 'grey'
  },
  {
    active:4,
    text: 'Diferido "Por Pagar"',
    color: 'purple'
  },
  {
    active:'',
    text: 'Todos'
  },
]

}
