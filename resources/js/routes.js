
import Register from './components/Register';
import Login from './components/Login.vue';
import Home from './components/Home';
import AdminHome from './components/Admin-home';

import Sales from './components/sales/index';
import Createsale from './components/sales/Create';
import Editsale from './components/sales/Edit';
import Historysale from './components/sales/History';

import Pay from './components/pay/index';
import Createpay from './components/pay/Create';
import Editpay from './components/pay/Edit';

import Banks from './components/banks/Index';

import Agents from './components/agents/Index';
import AgentUpdates from './components/agents/AgentUpdates';
import AgentPreferences from './components/agents/AgentPreferences';
import AgentProfile from './components/agents/Profile';

import Clients from './components/clients/Index';
import CreateClient from './components/clients/Create';
import ListClientsAgent from './components/clients/Sender';

import Prices from './components/prices/Index';
import Percents from './components/prices/Percent';
import Balances from './components/prices/Balance';

export const routes = [
    {
      path : '/access/login',
      name : 'login',
      component : Login,
      meta : {
        onlyLogout: true
      }
    },
    {
      path : '/access/registrar',
      name : 'register',
      component : Register,
      meta : {
        onlyLogout: true
      }
    },
    {
      path : '/home',
      name : 'home',
      component : Home,
    },
    {
      path : '/admin/',
      name : 'admin-home',
      component : AdminHome,
      meta:{
        requireAuth:true, admin:true, agent:false
      }
    },
    {
      path : '/admin/agents',
      name : 'agents',
      component : Agents,
      meta:{
        requireAuth:true, admin:true, agent:false
      }
    },
    {
      path : '/admin/agent-updates',
      name : 'agent-updates',
      component : AgentUpdates,
      meta:{
        requireAuth:true, admin:true, agent:false
      }
    },
    {
      path : '/admin/balance',
      name : 'balances',
      component : Balances,
      meta:{
        requireAuth:true, admin:true, agent:false
      }
    },
    {
      path : '/admin/clients',
      name : 'clients',
      component : Clients,
      meta:{
        requireAuth:true, admin:true, agent:false
      }
    },
    {
      path : '/admin/registrar-cliente/',
      name : 'create-client-admin',
      component : CreateClient,
      meta:{
        requireAuth:true, admin:true, agent:false
      }
    },
    {
      path : '/admin/banks',
      name : 'banks',
      component : Banks,
      meta:{
        requireAuth:true, admin:true, agent:false
      }
    },
    {
      path : '/admin/sales',
      name : 'sales',
      component : Sales,
      meta:{
        requireAuth:true, admin:true, agent:false
      }
    },
    {
      path : '/admin/sales/create',
      name : 'create-sale',
      component : Createsale,
      meta:{
        requireAuth:true, admin:true, agent:false
      }

    },
    {
      path : '/admin/sales/edit/:id',
      name : 'edit-sale',
      component : Editsale,
      meta:{
        requireAuth:true, admin:true, agent:false
      }
    },
    {
      path : '/admin/sales/history',
      name : 'sales-history',
      component : Historysale,
      meta:{
        requireAuth:true, admin:true, agent:false
      }
    },
    {
        path : '/admin/prices/',
        name : 'prices',
        component : Prices,
        meta:{
          requireAuth:true, admin:true, agent:false
        }
    },
    {
        path : '/admin/percent/',
        name : 'percent',
        component : Percents,
        meta:{
          requireAuth:true, admin:true, agent:false
        }
    },
    {
        path : '/agents/',
        name : 'agent-home',
        component : Home,
        meta:{
            requireAuth:true, admin:false, agent:true
        }
    },
    {
        path : '/agents/pagos/',
        name : 'pay',
        component : Pay,
        meta:{
          requireAuth:true, admin:false, agent:true
          }
    },
    {
        path : '/agents/pagos/registrar',
        name : 'create-pay',
        component : Createpay,
        meta:{
          requireAuth:true, admin:false, agent:true
          }
    },
    {
        path : '/agents/pagos/editar/:id',
        name : 'edit-pay',
        component : Editpay,
        meta:{
          requireAuth:true, admin:false, agent:true
          }
    },
    {
        path : '/agents/registrar-cliente/',
        name : 'create-client',
        component : CreateClient,
        meta:{
          requireAuth:true, admin:false, agent:true
        }
    },
    {
        path : '/agents/clientes/',
        name : 'clients-agent',
        component : ListClientsAgent,
        meta:{
          requireAuth:true, admin:false, agent:true
        }
    },
    {
        path : '/agents/profile/',
        name : 'agent-profile',
        component : AgentProfile,
        meta:{
          requireAuth:true, admin:false, agent:true
        }
    },
];


