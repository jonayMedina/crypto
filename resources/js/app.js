require('./bootstrap');

import Vue from 'vue';
window.Vue = require('vue');
import Vuetify from 'vuetify';
import VueClipboard from 'vue-clipboard2';
import '@mdi/font/css/materialdesignicons.css';
import VueRouter from 'vue-router';
import {routes} from './routes.js';
import MainApp from './components/MainApp';
import Alerts from './components/vue-helpers/Alerts'
import Vuex from 'vuex';
import store from './store';
import {initialize} from './helpers/general';
import Sfire from './helpers/swalfire';
import VueAxios from 'vue-axios';
import axios from 'axios';

import Swal from 'sweetalert2';

import { Form, HasError, AlertError } from 'vform';

window.Form = Form;
window.Swal = Swal;
window.Sfire = Sfire;
// Vue.component('vue-select', vSelect)
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)
Vue.use(VueAxios, axios);
Vue.use(VueClipboard);
Vue.use(Vuetify);
Vue.use(VueRouter);
Vue.use(Vuex);
// Vue.component('sfire',Sfire);
Vue.component('alerts', Alerts)

const router = new VueRouter({
    mode: 'history',
    routes,
});

router.beforeEach((to, from, next) => {

  let currentUser = store.getters.currentUser;
  if (!currentUser && to.meta.onlyLogout) {
    next();
  } else if (to.meta.requireAuth && currentUser) {

    if (currentUser.role==1 && to.meta.agent) {
      next();
    } else if (currentUser.role==1 && to.meta.admin ) {
      router.push({name: 'agent-home'});
    } else if (currentUser.role==3 && to.meta.admin) {
      next();
    }
  } else if (currentUser.role==1 && to.meta.onlyLogout) {
    router.push({name: 'agent-home'});
  } else if (currentUser.role==3 && to.meta.onlyLogout) {
    router.push({name: 'admin-home'})
  }
});

initialize(store,router);
const app = new Vue({
	el: '#admin',
	store,
	router,
    vuetify: new Vuetify(),
    iconfont: 'mdi',
	components : {
		MainApp
	},


});

